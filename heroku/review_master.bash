#!/bin/bash -ex

PROJECT_NAME='students'

curl -X POST https://api.heroku.com/apps -H "Accept: application/vnd.heroku+json; version=3" -H "Authorization: Bearer $HEROKU_API_KEY" -H "Content-Type: application/json" -d "{\"name\":\"$PROJECT_NAME-master\",\"region\":\"eu\"}"

git checkout -B $PROJECT_NAME-master
git status
git config user.email 'no-reply@universis.io'
git config user.name 'UniverSIS Project'

sed -i -e "s/\"callbackURL.*,/\"callbackURL\": \"https:\/\/$PROJECT_NAME-master.herokuapp.com\/auth\/callback\/index.html\",/" src/assets/config/app.json

sed -i -e "s/\"logoutURL.*,/\"logoutURL\":\"https:\/\/users.universis.io\/logout?continue=https:\/\/$PROJECT_NAME-master.herokuapp.com\/#\/auth\/login\",/" src/assets/config/app.json

sed -i -e "s/\"clientID.*,/\"clientID\": \"$CLIENT_ID\",/"  src/assets/config/app.json

cp src/assets/config/app.json src/assets/config/app.production.json
cp src/assets/config/app.production.json src/assets/config/app.development.json


git add -f src/assets/config/app.production.json
git add .
git commit -m "Ready to push latest changes to new heroku review app"
git push -f https://heroku:$HEROKU_API_KEY@git.heroku.com/$PROJECT_NAME-master.git $PROJECT_NAME-master:master
